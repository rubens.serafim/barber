package rsn.com.api.configs.infra.security.models;

import rsn.com.api.models.enums.UsuarioRole;

public record RegisterDTO(String email, String senha, UsuarioRole role) {
}
