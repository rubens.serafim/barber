package rsn.com.api.dtos.empresa;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class EmpresaDto {
    private String uuid;
    private String nome;
    private String email;
    private String cnpj;
    private String endereco;
    private String telefone;
    private String status;
    private String token;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataExclusao;
}
