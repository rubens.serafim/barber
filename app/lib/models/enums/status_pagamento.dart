enum StatusPagamento {
  INICIADO,
  FINALIZADO,
  CANCELADO,
  CORTESIA,
}

extension StatusPagamentoExtension on StatusPagamento {
  String get descricao {
    switch (this) {
      case StatusPagamento.INICIADO:
        return 'INICIADO';
      case StatusPagamento.FINALIZADO:
        return 'FINALIZADO';
      case StatusPagamento.CANCELADO:
        return 'CANCELADO';
      case StatusPagamento.CORTESIA:
        return 'CORTESIA';
      default:
        return '';
    }
  }

  static StatusPagamento fromString(String status) {
    switch (status) {
      case 'INICIADO':
        return StatusPagamento.INICIADO;
      case 'FINALIZADO':
        return StatusPagamento.FINALIZADO;
      case 'CANCELADO':
        return StatusPagamento.CANCELADO;
      case 'CORTESIA':
        return StatusPagamento.CORTESIA;
      default:
        throw ArgumentError('Invalid status string: $status');
    }
  }
}
