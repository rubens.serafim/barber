package rsn.com.api.configs.infra.security.models;

public record LoginResponseDTO(String token) {
}
