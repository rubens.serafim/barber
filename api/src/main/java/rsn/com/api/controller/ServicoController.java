package rsn.com.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rsn.com.api.models.atendimento.AtendimentoModel;
import rsn.com.api.models.servico.ServicoModel;
import rsn.com.api.service.atendimento.AtendimentoServiceImpl;
import rsn.com.api.service.servico.ServicoServiceImpl;

@RestController
@RequestMapping("/servicos")
public class ServicoController {

    private final ServicoServiceImpl servicoService;
    private final AtendimentoServiceImpl atendimentoService;

    @Autowired
    public ServicoController(ServicoServiceImpl servicoService,
            AtendimentoServiceImpl atendimentoService) {
        this.servicoService = servicoService;
        this.atendimentoService = atendimentoService;

    }

    @PutMapping("/atualizar")
    public ResponseEntity<ServicoModel> atualizarServico(
            @RequestParam String uuid,
            @RequestBody ServicoModel servico) {
        ServicoModel servicoAtualizado = servicoService.atualizarServico(servico);

        atendimentoService.finalizaAtendimentoBarbeiro(uuid, servicoAtualizado.getUuid());

        return ResponseEntity.status(HttpStatus.OK).body(servicoAtualizado);
    }
}
