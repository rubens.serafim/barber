package rsn.com.api.configs;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import java.util.List;
import java.util.ArrayList;

import lombok.extern.slf4j.Slf4j;

/**
 * Manipulador de mensagens para WebSocket.
 */
@Slf4j
public class ManipuladorMensagens implements WebSocketHandler {

    /** Lista de sessões WebSocket ativas. */
    private static final List<WebSocketSession> sessions = new ArrayList<>();

    /**
     * Método chamado quando uma nova conexão WebSocket é estabelecida.
     *
     * @param session A sessão WebSocket recém-estabelecida.
     * @throws Exception Se ocorrer um erro durante o processamento.
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.info("Conexão estabelecida na sessão: {}", session.getId());
        sessions.add(session);
    }

    /**
     * Método chamado quando uma mensagem é recebida em uma sessão WebSocket.
     *
     * @param session A sessão WebSocket na qual a mensagem foi recebida.
     * @param message A mensagem recebida.
     * @throws Exception Se ocorrer um erro durante o processamento.
     */
    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        String status = (String) message.getPayload();
        log.info("Status: {}", status);
        for (WebSocketSession sess : sessions) {
            sess.sendMessage(new TextMessage("Processamento do status iniciado: " + session + " - " + status));
            Thread.sleep(1000);
            sess.sendMessage(new TextMessage("Processamento do status completo: " + status));
        }
    }

    /**
     * Método chamado quando ocorre um erro de transporte em uma sessão WebSocket.
     *
     * @param session   A sessão WebSocket na qual o erro ocorreu.
     * @param exception O erro de transporte.
     * @throws Exception Se ocorrer um erro durante o processamento.
     */
    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        log.info("Ocorreu uma exceção: {} na sessão: {}", exception.getMessage(), session.getId());
    }

    /**
     * Método chamado quando uma conexão WebSocket é fechada.
     *
     * @param session     A sessão WebSocket que foi fechada.
     * @param closeStatus O status do fechamento da conexão.
     * @throws Exception Se ocorrer um erro durante o processamento.
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        log.info("Conexão fechada na sessão: {} com status: {}", session.getId(), closeStatus.getCode());
        sessions.remove(session);
    }

    /**
     * Indica se este manipulador de mensagens suporta mensagens parciais.
     *
     * @return true se este manipulador suportar mensagens parciais, caso contrário,
     *         false.
     */
    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
