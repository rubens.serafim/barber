package rsn.com.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rsn.com.api.models.pagamento.PagamentoModel;

@Repository
public interface PagamentoRepository extends JpaRepository<PagamentoModel, String> {
}
