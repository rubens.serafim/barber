package rsn.com.api.dtos.email;
import lombok.Data;

@Data
public class EmailDto {
    private String uuid;
    private String nome;
    private String subject;
    private String text;
}
