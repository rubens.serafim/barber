import 'dart:convert';

import 'package:app/models/enums/UsuarioRole.dart';

class Usuario {
  String uuid;
  String nome = '';
  String cpf;
  String celular;
  String email;
  String senha;
  String foto;
  String status;
  UsuarioRole role;
  DateTime dataCriacao;
  DateTime? dataExclusao;
  String quantidadeClientes = "0";

  Usuario({
    required this.uuid,
    required this.nome,
    required this.cpf,
    required this.celular,
    required this.email,
    required this.senha,
    required this.foto,
    required this.status,
    required this.role,
    required this.dataCriacao,
    this.dataExclusao,
    String? quantidadeClientes,
  });

  factory Usuario.fromJson(Map<String, dynamic> json) {
  return Usuario(
    uuid: json['uuid'] ?? '',  // Valor padrão para evitar null
    nome: json['nome'] ?? '',  // Valor padrão para string vazia
    cpf: json['cpf'] ?? '',  // Valor padrão para CPF
    celular: json['celular'] ?? '',  // Valor padrão para celular
    email: json['email'] ?? '',  // Valor padrão para email
    senha: json['senha'] ?? '',  // Valor padrão para senha
    foto: json['foto'] ?? '',  // Valor padrão para foto
    status: json['status'] ?? 'ativo',  // Valor padrão para status
    role: usuarioRoleFromString(json['role'] ?? 'USUARIO'),  // Valor padrão para role
    dataCriacao: json['dataCriacao'] != null 
        ? DateTime.parse(json['dataCriacao']) 
        : DateTime.now(),  // Se null, utiliza a data atual
    dataExclusao: json['dataExclusao'] != null 
        ? DateTime.parse(json['dataExclusao']) 
        : null,  // Se null, mantém como null
    quantidadeClientes: json['quantidadeClientes'] ?? '0',  // Valor padrão para quantidade de clientes
  );
}


  Map<String, dynamic> toJson() {
    return {
      'uuid': uuid,
      'nome': nome,
      'cpf': cpf,
      'celular': celular,
      'email': email,
      'senha': senha,
      'foto': foto,
      'status': status,
      'role': usuarioRoleToString(role),
      'dataCriacao': dataCriacao.toIso8601String(),
      'dataExclusao': dataExclusao?.toIso8601String(),
      'quantidadeClientes': quantidadeClientes,
    };
  }

  void updateFromJson(Map<String, dynamic> userData) {
    if (userData.containsKey('nome')) nome = userData['nome'];
    if (userData.containsKey('cpf')) cpf = userData['cpf'];
    if (userData.containsKey('celular')) celular = userData['celular'];
    if (userData.containsKey('email')) email = userData['email'];
    if (userData.containsKey('senha')) senha = userData['senha'];
    if (userData.containsKey('foto')) foto = userData['foto'];
    if (userData.containsKey('status')) status = userData['status'];
    if (userData.containsKey('role'))
      role = usuarioRoleFromString(userData['role']);
    if (userData.containsKey('dataCriacao'))
      dataCriacao = DateTime.parse(userData['dataCriacao']);
    if (userData.containsKey('dataExclusao'))
      dataExclusao = DateTime.parse(userData['dataExclusao']);
    if (userData.containsKey('quantidadeClientes'))
      quantidadeClientes = userData['quantidadeClientes'];
  }
  
  // Método para converter a parte do payload do JWT em um objeto Usuario
  static Usuario fromToken(String token) {
    // Parte do payload no JWT é a segunda parte (base64Url)
    String normalizedSource = base64Url.normalize(token.split(".")[1]);
    String decodedJson = utf8.decode(base64Url.decode(normalizedSource));

    // Decodificar o JSON para Map<String, dynamic>
    Map<String, dynamic> jsonData = jsonDecode(decodedJson);

    // Retornar um objeto Usuario a partir dos dados decodificados
    return Usuario.fromJson(jsonData);
  }
}
