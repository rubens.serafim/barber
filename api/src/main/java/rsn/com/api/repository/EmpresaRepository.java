package rsn.com.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import rsn.com.api.models.empresa.EmpresaModel;

public interface EmpresaRepository extends JpaRepository<EmpresaModel, String> {

    Optional<EmpresaModel> findByCnpj(String cnpj);

    /*
     * Método para buscar uma empresa pelo ID
     */
    Optional<EmpresaModel> findById(String id);

    /*
     * Método para buscar todas as empresas
     */
    List<EmpresaModel> findAll();

    /*
     * Método para excluir uma empresa pelo ID
     */
    void deleteById(String id);

}
