class QtdClientePorBarberDto {
  final String fkUsuarioBarbeiro;
  final int quantidadeClientes;

  QtdClientePorBarberDto(this.fkUsuarioBarbeiro, this.quantidadeClientes);

  factory QtdClientePorBarberDto.fromJson(Map<String, dynamic> json) {
    return QtdClientePorBarberDto(
      json['fkUsuarioBarbeiro'] as String,
      json['quantidadeClientes'] as int,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'fkUsuarioBarbeiro': fkUsuarioBarbeiro,
      'quantidadeClientes': quantidadeClientes,
    };
  }
}
