package rsn.com.api.service.atendimento;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rsn.com.api.dtos.atendimento.AtendimentosDto;
import rsn.com.api.dtos.atendimento.BarberWithClientsDto;
import rsn.com.api.dtos.atendimento.QtdClientePorBarberDto;
import rsn.com.api.exceptions.AtendimentoNotFoundException;
import rsn.com.api.models.atendimento.AtendimentoModel;
import rsn.com.api.models.enums.StatusAtendimento;
import rsn.com.api.models.enums.StatusPagamento;
import rsn.com.api.models.pagamento.PagamentoModel;
import rsn.com.api.models.servico.ServicoModel;
import rsn.com.api.models.usuario.UsuarioModel;
import rsn.com.api.repository.AtendimentoRepository;
import rsn.com.api.repository.PagamentoRepository;
import rsn.com.api.repository.ServicoRepository;
import rsn.com.api.repository.ServicoRepository;
import rsn.com.api.repository.UsuarioRepository;

import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDateTime;

@Service
public class AtendimentoServiceImpl {

    private final ServicoRepository servicoRepository;
    private final PagamentoRepository pagamentoRepository;
    private final AtendimentoRepository atendimentoRepository;
    private final UsuarioRepository usuarioRepository;

    @Autowired
    public AtendimentoServiceImpl(ServicoRepository servicoRepository,
            PagamentoRepository pagamentoRepository,
            AtendimentoRepository atendimentoRepository,
            UsuarioRepository usuarioRepository) {
        this.servicoRepository = servicoRepository;
        this.pagamentoRepository = pagamentoRepository;
        this.atendimentoRepository = atendimentoRepository;
        this.usuarioRepository = usuarioRepository;
    }

    @Transactional
    public AtendimentoModel save(String idBarbeiro, String idCliente) {
        UsuarioModel barbeiro = usuarioRepository.findById(idBarbeiro)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Barbeiro não encontrado com o ID: " + idBarbeiro));

        UsuarioModel cliente = usuarioRepository.findById(idCliente)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cliente não encontrado com o ID: " + idCliente));
        // Adicionar um método para verificar se há cliente na fila na data atual e
        // verificar ser o status dele é diferente de ATENDIDO, caso contrário não cria
        // o atendimento

        AtendimentoModel atendimento = new AtendimentoModel();
        atendimento.setUsuarioBarber(barbeiro);
        atendimento.setUsuarioCliente(cliente);
        atendimento.setStatus(StatusAtendimento.AGUARDANDO);

        return atendimentoRepository.save(atendimento);
    }

    @Transactional
    public void atualizarAtendimento(String uuid) {
        AtendimentoModel atendimento = atendimentoRepository.findById(uuid)
                .orElseThrow(() -> new AtendimentoNotFoundException(
                        "Atendimento não encontrado com o ID: " + uuid));

        // Definindo a data e hora atual para dataHoraFimAtend
        atendimento.setDataHoraInicioAtendimento(LocalDateTime.now());

        // Definindo o status para "ATENDIMENTO"
        atendimento.setStatus(StatusAtendimento.ATENDIMENTO);

        // Salvando as alterações no banco de dados
        atendimentoRepository.save(atendimento);
    }

    @Transactional
    public AtendimentoModel finalizaAtendimentoBarbeiro(String uuidAtendimento, String uuidServico) {

        ServicoModel servicoModel = servicoRepository.findById(uuidServico)
                .orElseThrow(() -> new AtendimentoNotFoundException(
                        "Atendimento não encontrado com o ID: " + uuidAtendimento));

        AtendimentoModel atendimento = atendimentoRepository.findById(uuidAtendimento)
                .orElseThrow(() -> new AtendimentoNotFoundException(
                        "Atendimento não encontrado com o ID: " + uuidAtendimento));

        PagamentoModel pagamento = new PagamentoModel();
        pagamento.setServico(servicoModel);
        pagamento.setValor(servicoModel.getValor());
        pagamento.setStatus(StatusPagamento.INICIADO);

        // Atualiza o pagamento
        PagamentoModel pagamentoAtualizado = pagamentoRepository.save(pagamento);

        // Atualiza o atendimento
        atendimento.setDataHoraFimAtendimento(LocalDateTime.now());
        atendimento.setStatus(StatusAtendimento.ATENDIDO);
        atendimento.setPagamento(pagamentoAtualizado);

        return atendimentoRepository.save(atendimento);
    }

    public List<AtendimentosDto> findAllAtendimentos() {
        List<AtendimentoModel> atendimentos = atendimentoRepository.findAll();
        return atendimentos.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private AtendimentosDto convertToDto(AtendimentoModel atendimento) {
        AtendimentosDto dto = new AtendimentosDto();
        dto.setUuid(atendimento.getUuid());
        dto.setPagamento(atendimento.getPagamento());
        dto.setUsuarioBarber(atendimento.getUsuarioBarber());
        dto.setUsuarioCliente(atendimento.getUsuarioCliente());
        dto.setDataHoraInicio(atendimento.getDataHoraInicio());
        dto.setDataHoraInicioAtendimento(atendimento.getDataHoraInicioAtendimento());
        dto.setDataHoraFimAtendimento(atendimento.getDataHoraFimAtendimento());
        dto.setDataHoraFimPagamento(atendimento.getDataHoraFimPagamento());
        dto.setStatus(atendimento.getStatus());
        return dto;
    }

    public List<QtdClientePorBarberDto> countClientesPorBarbeiro() {
        List<QtdClientePorBarberDto> resultados = atendimentoRepository.countClientesPorBarbeiros();

        return resultados.stream()
                .map(dto -> {
                    Long quantidadeClientes = dto.getQuantidadeClientes();
                    dto.setQuantidadeClientes(quantidadeClientes);
                    return dto;
                })
                .collect(Collectors.toList());
    }

    public boolean usuarioClienteExistsInAtendimento(UsuarioModel usuarioCliente) {
        return atendimentoRepository.existsByUsuarioCliente(usuarioCliente);
    }

    public List<AtendimentoModel> getAtendimentosByUsuarioClienteId(String usuarioClienteId) {
        return atendimentoRepository.findByUsuarioClienteUuid(usuarioClienteId);
    }
    public List<UsuarioModel> findAslBarbersWithClienats() {
        List<UsuarioModel> barbers = atendimentoRepository.findAllDistinctBarbers();
        return barbers;
    }
   
    // public List<BarberWithClientsDto> findAllBarbersWithClients() {
    //     List<UsuarioModel> barbers = atendimentoRepository.findAllDistinctBarbers();

    //     return barbers.stream()
    //         .map(barber -> {
    //             List<UsuarioModel> clients = atendimentoRepository.findAllByBarberId(barber.getUuid())
    //                 .stream()
    //                 .map(AtendimentoModel::getUsuarioCliente)
    //                 .distinct()
    //                 .collect(Collectors.toList());
    //             return new BarberWithClientsDto(barber, clients);
    //         })
    //         .collect(Collectors.toList());
    //     }
}
