package rsn.com.api.exceptions;

public class UsuarioNotFoundException  extends RuntimeException {

    public UsuarioNotFoundException(String cpf) {
        super("Já existe uma empresa cadastrada com o CNPJ fornecido: " + cpf);
    }
}
