package rsn.com.api.models.enums;

public enum StatusAtendimento {

    
    AGUARDANDO("AGUARDANDO"),
    ATENDIMENTO("ATENDIMENTO"),
    ATENDIDO("ATENDIDO"),
    DESISTIU("DESISTIU");

	/**
	 * Atributo que exibe o texto relacionado a constante do enum
	 */
    private final String descricao;

	/**
	 * Construtor do enum
	 *
	 * @param descricao
	 */
    StatusAtendimento(String descricao) {
        this.descricao = descricao;
    }

	/**
     * Getter descricao
     *
     * @return
     */
    public String getDescricao() {
        return descricao;
    }
    
}
