package rsn.com.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import rsn.com.api.dtos.atendimento.AtendimentosDto;
import rsn.com.api.dtos.atendimento.BarberWithClientsDto;
import rsn.com.api.dtos.atendimento.QtdClientePorBarberDto;
import rsn.com.api.models.atendimento.AtendimentoModel;
import rsn.com.api.models.servico.ServicoModel;
import rsn.com.api.models.usuario.UsuarioModel;
import rsn.com.api.service.atendimento.AtendimentoServiceImpl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador para manipular requisições relacionadas a atendimentos.
 */
@RestController
@RequestMapping("/atendimentos")
public class AtendimentoController {

    private final AtendimentoServiceImpl atendimentoService;

    public AtendimentoController(AtendimentoServiceImpl atendimentoService) {
        this.atendimentoService = atendimentoService;
    }

    /**
     * Endpoint para criar um novo atendimento.
     *
     * @param idBarbeiro ID do barbeiro responsável pelo atendimento.
     * @param idCliente  ID do cliente sendo atendido.
     * @return Resposta HTTP com o atendimento criado e status 201 (Created).
     */
    @PostMapping("/criar-atendimentos")
    public ResponseEntity<AtendimentoModel> createAtendimento(@RequestParam("idBarbeiro") String idBarbeiro,
            @RequestParam("idCliente") String idCliente) {
        AtendimentoModel savedAtendimento = atendimentoService.save(idBarbeiro, idCliente);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedAtendimento);
    }

    /**
     * Endpoint para atualizar um atendimento.
     *
     * @param uuid UUID do atendimento a ser atualizado.
     * @return Resposta HTTP com mensagem de sucesso e status 200 (OK).
     */
    @PutMapping("/atualizar")
    public ResponseEntity<String> atualizarAtendimento(@RequestParam String uuid) {
        atendimentoService.atualizarAtendimento(uuid);
        return ResponseEntity.status(HttpStatus.OK).body("Atendimento atualizado com sucesso");
    }

    @GetMapping
    public List<AtendimentosDto> getAllAtendimentos() {
        return atendimentoService.findAllAtendimentos();
    }

    // @GetMapping("/quantidade")
    // public ResponseEntity<List<UsuarioModel>> countClientesPorBarbeiro() {
    // List<UsuarioModel> usuarios = atendimentoService.countClientesPorBarbeiro();
    // return ResponseEntity.ok(usuarios);
    // }

    /**
     * Endpoint para finalizar um atendimento.
     *
     * @param uuid         UUID do atendimento a ser finalizado.
     * @param serviceModel Modelo de serviço associado ao atendimento.
     * @return Resposta HTTP com o atendimento atualizado e status 200 (OK).
     */
    // @PutMapping("/finalizar")
    // public ResponseEntity<AtendimentoModel> atualizarTabelasRelacionadas(
    // @RequestParam String uuid,
    // @RequestBody ServicoModel serviceModel) {

    // AtendimentoModel atendimentoAtualizado =
    // atendimentoService.finalizaAtendimentoBarbeiros(uuid, serviceModel);
    // return ResponseEntity.status(HttpStatus.OK).body(atendimentoAtualizado);
    // }

    @GetMapping("/contagem-clientes-barbeiros")
    public List<QtdClientePorBarberDto> contagemClientesPorBarbeiro() {
        return atendimentoService.countClientesPorBarbeiro();
    }

    @GetMapping("/exists/{usuarioClienteId}")
    public boolean usuarioClienteExistsInAtendimento(@PathVariable String usuarioClienteId) {
        UsuarioModel usuarioCliente = new UsuarioModel();
        usuarioCliente.setUuid(usuarioClienteId);
        return atendimentoService.usuarioClienteExistsInAtendimento(usuarioCliente);
    }

    @GetMapping("/usuario/{usuarioClienteId}")
    public ResponseEntity<List<AtendimentoModel>> getAtendimentosByUsuarioClienteId(
            @PathVariable String usuarioClienteId) {
        List<AtendimentoModel> atendimentos = atendimentoService.getAtendimentosByUsuarioClienteId(usuarioClienteId);
        return ResponseEntity.ok(atendimentos);
    }

    // @GetMapping("/barbers-with-clients")
    // public List<BarberWithClientsDto> getAllBarbersWithClients() {
    //     return atendimentoService.findAllBarbersWithClients();
    // }


    @GetMapping("/barbers-with-clients")
    public List<UsuarioModel> getAllBarbersWithClients() {
        return atendimentoService.findAslBarbersWithClienats();
    }
}
