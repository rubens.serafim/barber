package rsn.com.api.models.pagamento;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import org.hibernate.annotations.GenericGenerator;

import rsn.com.api.models.enums.StatusPagamento;
import rsn.com.api.models.servico.ServicoModel;
import rsn.com.api.models.usuario.UsuarioModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tb_pagamento")
public class PagamentoModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "pk_pagamento", updatable = false, nullable = false)
    private String uuid;

    @ManyToOne
    @JoinColumn(name = "fk_servico")
    private ServicoModel servico;

    // @ManyToOne
    // @JoinColumn(name = "fk_usuario_cliente")
    // private UsuarioModel usuarioCliente;

    // @ManyToOne
    // @JoinColumn(name = "fk_usuario_barber")
    // private UsuarioModel usuarioBarber;

    @Column(name = "data_hora", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime dataHora;

    @Column(name = "valor", nullable = false)
    private BigDecimal valor;


    @Column(name = "status", nullable = false)
    private StatusPagamento status;
}
