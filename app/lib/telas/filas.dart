import 'dart:convert';

import 'package:app/models/Usuario.dart';
import 'package:app/models/atendimento.dart';
import 'package:app/models/dtos/QtdClientePorBarberDto.dart';
import 'package:app/models/enums/UsuarioRole.dart';
import 'package:app/service/AtendimentoService.dart';
import 'package:app/service/UsuarioService.dart';
import 'package:app/service/authStorage.dart';
import 'package:app/service/services.dart';
import 'package:app/telas/Home.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

import 'package:jwt_decoder/jwt_decoder.dart';

class Filas extends StatefulWidget {
  const Filas({super.key});

  @override
  State<Filas> createState() => _FilasState();
}

class _FilasState extends State<Filas> {
  bool _isLoading = false;

  List<Usuario> _usuariosBarbeiros = []; // Lista de usuários
  List<AtendimentoModel> _atendimentos = [];

  String uuid = '';
  Usuario _usuarioLogado = Usuario(
      uuid: '',
      nome: '',
      cpf: '',
      celular: '',
      email: '',
      senha: '',
      foto: '',
      status: '',
      role: UsuarioRole.USER,
      dataCriacao: DateTime.now());

  Usuario _usuarioa = Usuario(
      uuid: '',
      nome: '',
      cpf: '',
      celular: '',
      email: '',
      senha: '',
      foto: '',
      status: '',
      role: UsuarioRole.USER,
      dataCriacao: DateTime.now());
  Usuario _usuario = Usuario(
      uuid: '',
      nome: '',
      cpf: '',
      celular: '',
      email: '',
      senha: '',
      foto: '',
      status: '',
      role: UsuarioRole.USER,
      dataCriacao: DateTime.now());

  @override
  void initState() {
    super.initState();
    _decodeToken();
    _buscarUsuarioBarbeiro();
  }

  String decodeUserData(String code) {
    String normalizedSource = base64Url.normalize(code.split(".")[1]);
    return utf8.decode(base64Url.decode(normalizedSource));
  }

  _decodeToken() {
    // Pegar o token do armazenamento
    String? token = AuthStorage().token;
    print(token);


    // Verifica se o token é válido antes de tentar decodificá-lo
    if (token != null && token.isNotEmpty) {
      try {
        String userToken = decodeUserData(token);
        // Converte o token decodificado para o objeto Usuario
        Usuario usuarioLogado = Usuario.fromJson(jsonDecode(userToken));

    print(usuarioLogado);
    print(userToken);


        // Chama a função para pegar todos os atendimentos
        _getAllAtendimentos(usuarioLogado);

        // Atualiza o estado com o usuário logado
        setState(() {
          _usuarioLogado = usuarioLogado;
        });
      } catch (e) {
        print("Erro ao decodificar o token JWT: $e");
      }
    } else {
      print("Token não encontrado ou inválido.");
    }
  }

  Future<void> _getAllAtendimentos(usuarioLogado) async {
    final atendimentos = await AtendimentoService.getAllAtendimentos();

    // Verifica se _usuario está inicializado antes de prosseguir
    if (usuarioLogado == null) {
      return;
    }

    // Percorre cada atendimento para verificar os usuários
    atendimentos.forEach((atendimento) {
      if (atendimento.usuarioCliente.uuid == _usuario.uuid) {
        // Faça algo se encontrar o usuário no atendimento
        print('Usuário encontrado em um atendimento: ${atendimento.uuid}');
      }
    });

    // print('Usuário sistema: ${_usuario.uuid}');

    print(atendimentos);
    // print('Atendimento 2: ${atendimentos[1].usuarioCliente.uuid}');

    // Atualiza o estado com os atendimentos recebidos
    if (mounted) {
      setState(() {
        _atendimentos = atendimentos;
      });
    }
  }

  Future<void> _buscarUsuarioBarbeiro() async {
    if (!mounted)
      return; // Adicione essa linha para garantir que o widget ainda está montado

    setState(() {
      _isLoading = true; // Ative o indicador de carregamento
    });
    await UsuarioService.buscarUsuarioBarbeiro()
        .then((List<Usuario> usuarios) async {
      List<QtdClientePorBarberDto> qtdClientes =
          await UsuarioService.contagemClientesPorBarbeiro();

      usuarios.forEach((usuario) {
        QtdClientePorBarberDto? qtdClienteDoUsuario =
            qtdClientes.firstWhereOrNull(
          (qtdCliente) => qtdCliente.fkUsuarioBarbeiro == usuario.uuid,
        );

        if (qtdClienteDoUsuario == null) {
          usuario.quantidadeClientes = "0";
        } else {
          usuario.quantidadeClientes =
              qtdClienteDoUsuario.quantidadeClientes.toString();
        }
      });

      if (mounted) {
        setState(() {
          _usuariosBarbeiros = usuarios;
          _isLoading = false;
        });
      }
    }).catchError((error) {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Falha ao buscar os usuários: $error'),
          backgroundColor: Colors.red,
        ),
      );
      print('Erro ao obter usuários: $error');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            colors: [
              Colors.blue[900]!,
              Colors.blue[800]!,
              Colors.blue[400]!,
            ],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(
                    height: 10,
                  ),
                  const Center(
                    child: Text(
                      "Sistema Barbearia",
                      style: TextStyle(color: Colors.white, fontSize: 40),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Center(
                    child: Text(
                      'Bem vindo ${_usuarioLogado.nome}',
                      style: const TextStyle(color: Colors.white, fontSize: 10),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 5),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  ),
                ),
                child: _isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : ListView.builder(
                        itemCount: _usuariosBarbeiros.length,
                        itemBuilder: (context, index) {
                          final usuarioGeral = _usuariosBarbeiros.firstWhere(
                            (usuario) => usuario.nome == 'Geral',
                          );

                          final outrosUsuarios = _usuariosBarbeiros
                              .where(
                                (usuario) => usuario.nome != 'Geral',
                              )
                              .toList();

                          outrosUsuarios
                              .sort((a, b) => a.nome.compareTo(b.nome));

                          final usuariosOrdenados = [
                            if (usuarioGeral != null) usuarioGeral,
                            ...outrosUsuarios
                          ];

                          final usuario = usuariosOrdenados[index];

                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 4.0, horizontal: 12.0),
                            child: GestureDetector(
                              onTap: () {
                                _handleUsuarioTap(context, usuario);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue[50],
                                  borderRadius: BorderRadius.circular(12.0),
                                ),
                                child: ListTile(
                                  leading: const Icon(Icons.people_alt),
                                  subtitle: Text(usuario.status),
                                  title: Text(usuario.nome),
                                  trailing: Wrap(
                                    spacing: 12,
                                    children: <Widget>[
                                      Text(usuario.quantidadeClientes),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _handleUsuarioTap(BuildContext context, Usuario usuario) async {
    try {
      final ownerName = _usuario.nome;
      final clienteUuid = uuid;
      final barberUuid = usuario.uuid;
      final barberNome = usuario.nome;
      final barberQuantidadeClientes = usuario.quantidadeClientes;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => HomePage(
            ownerName: ownerName,
            clienteUuid: clienteUuid,
            barberUuid: barberUuid,
            barberQuantidadeClientes: barberQuantidadeClientes,
            barberNome: barberNome,
          ),
        ),
      );
    } catch (e) {
      print('Erro ao obter dados do token: $e');
    }
  }
}
