package rsn.com.api.exceptions;

public class CNPJAlreadyExistsException extends RuntimeException {

    public CNPJAlreadyExistsException(String cnpj) {
        super("Já existe uma empresa cadastrada com o CNPJ fornecido: " + cnpj);
    }
}
