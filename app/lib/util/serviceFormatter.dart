import 'package:flutter/services.dart';

class ServiceFormatter {
  static String cpfInputFormatter(String cpf) {
    final RegExp regex = RegExp(r'(\d{0,3})(\d{0,3})(\d{0,3})(\d{0,2})');
    final StringBuffer result = StringBuffer();

    if (cpf.length <= 11) {
      for (int i = 0; i < cpf.length; i++) {
        if (i == 3 || i == 6) {
          result.write('.');
        } else if (i == 9) {
          result.write('-');
        }
        result.write(cpf[i]);
      }
    } else {
      result.write(cpf.substring(0, 11));
      result.write('-');
      result.write(cpf.substring(11, cpf.length));
    }

    return result.toString();
  }

  static String celularInputFormatter(String celular) {
    final RegExp regex = RegExp(r'(\d{0,2})(\d{0,5})(\d{0,4})');
    final StringBuffer result = StringBuffer();

    if (celular.length <= 2) {
      for (int i = 0; i < celular.length; i++) {
        if (i == 0) {
          result.write('(');
        }
        result.write(celular[i]);
      }
    } else if (celular.length <= 7) {
      result.write('(');
      for (int i = 0; i < celular.length; i++) {
        if (i == 2) {
          result.write(')');
        }
        result.write(celular[i]);
      }
    } else {
      result.write('(');
      result.write(celular.substring(0, 2));
      result.write(')');
      result.write(celular.substring(2, 7));
      result.write('-');
      result.write(celular.substring(7, celular.length));
    }

    return result.toString();
  }

  static bool validateEmail(String email) {
    // Expressão regular para validar o formato do e-mail
    final RegExp regex = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
    return regex.hasMatch(email);
  }
}
