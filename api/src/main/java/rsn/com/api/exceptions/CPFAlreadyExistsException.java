package rsn.com.api.exceptions;

public class CPFAlreadyExistsException extends RuntimeException {

    public CPFAlreadyExistsException(String cpf) {
        super("Já existe uma empresa cadastrada com o CNPJ fornecido: " + cpf);
    }
}
