package rsn.com.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.UserDetails;

import rsn.com.api.models.enums.UsuarioRole;
import rsn.com.api.models.usuario.UsuarioModel;

public interface UsuarioRepository extends JpaRepository<UsuarioModel, String> {

    /**
     * Consulta para buscar usuários por role ordenados por nome em ordem
     * decrescente
     * 
     * @param role regra 
     * @return List <UsuarioModel>
     */
    @Query("SELECT u FROM UsuarioModel u " +
            "WHERE u.role = :role OR u.nome = 'Geral' " +
            "ORDER BY u.nome DESC")
    List<UsuarioModel> findByRoleOrderByNomeDesc(@Param("role") UsuarioRole role);

    Optional<UsuarioModel> findByCpf(String cpf);

    UserDetails findByEmail(String email);

    Optional<UsuarioModel> findById(String id);

    List<UsuarioModel> findAll();

    void deleteById(String id);

}
