package rsn.com.api.models.atendimento;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;

import rsn.com.api.models.enums.StatusAtendimento;
import rsn.com.api.models.pagamento.PagamentoModel;
import rsn.com.api.models.usuario.UsuarioModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tb_atendimento")
public class AtendimentoModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "pk_atendimento", updatable = false, nullable = false)
    private String uuid;

    @ManyToOne
    @JoinColumn(name = "fk_pagamento")
    private PagamentoModel pagamento;

    @ManyToOne
    @JoinColumn(name = "fk_usuario_barber")
    private UsuarioModel usuarioBarber;

    @ManyToOne
    @JoinColumn(name = "fk_usuario_cliente")
    private UsuarioModel usuarioCliente;

    @Column(name = "data_hora_inicio", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime dataHoraInicio;

    @Column(name = "data_hora_inicio_atend")
    private LocalDateTime dataHoraInicioAtendimento;

    @Column(name = "data_hora_fim_atend")
    private LocalDateTime dataHoraFimAtendimento;

    @Column(name = "data_hora_fim_pagamento")
    private LocalDateTime dataHoraFimPagamento;

    @Column(name = "status", nullable = false)
    private StatusAtendimento status;
}
