package rsn.com.api.dtos.atendimento;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import rsn.com.api.models.usuario.UsuarioModel;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BarberWithClientsDto {
    private UsuarioModel usuarioBarber;
    private List<UsuarioModel> usuarioClientes;
}
