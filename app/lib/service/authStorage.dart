class AuthStorage {
  static AuthStorage? _instance;
  String? _token;

  factory AuthStorage() {
    if (_instance == null) {
      _instance = AuthStorage._();
    }
    return _instance!;
  }

  AuthStorage._();

  String? get token => _token;

  void setToken(String? token) {
    _token = token;
  }
}
