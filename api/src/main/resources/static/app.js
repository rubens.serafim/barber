'use strict';


/**
 * URL do WebSocket para conexão com o servidor.
 * @type {string}
 */
const url = "ws://localhost:8081/websocket";

/**
 * Cliente STOMP - Protocolo de Mensagens Orientado a Texto Simples" para comunicação com o servidor WebSocket.
 * @type {object}
 */
const clienteStomp = new StompJs.Client({
    brokerURL: url
});


// Definindo a função a ser executada quando a conexão é estabelecida
clienteStomp.onConnect = (frame) => {
    // Configura o estado de conexão como verdadeiro
    setConectado(true);
    console.log('Conectado: ' + frame);
    // Assinando o tópico '/topic/app' para receber mensagens
    clienteStomp.subscribe('/topic/app', (saudacao) => {
        // Mostrando as saudações recebidas
        mostrarSaudacao(JSON.parse(saudacao.body).content);
    });
};

// Definindo a função a ser executada em caso de erro no WebSocket
clienteStomp.onWebSocketError = (erro) => {
    console.error('Erro com o websocket', erro);
};

// Definindo a função a ser executada em caso de erro reportado pelo broker
clienteStomp.onStompError = (frame) => {
    console.error('Erro relatado pelo broker: ' + frame.headers['message']);
    console.error('Detalhes adicionais: ' + frame.body);
};

// Função para configurar o estado de conexão
function setConectado(conectado) {
    // Configura o botão de conectar como desabilitado se conectado, senão habilita
    $("#conectar").prop("disabled", conectado);
    // Configura o botão de desconectar como desabilitado se não conectado, senão habilita
    $("#desconectar").prop("disabled", !conectado);
    // Mostra ou esconde a seção de conversa dependendo do estado de conexão
    if (conectado) {
        $("#conversa").show();
    } else {
        $("#conversa").hide();
    }
    // Limpa as saudações
    $("#saudacoes").html("");
}

// Função para conectar ao WebSocket
function conectar() {
    console.log("AAAAAAAAAAAAAAA");
    // Ativa o cliente Stomp
    clienteStomp.activate();
    // Executa a função de conexão
    clienteStomp.onConnect();
}

// Função para desconectar do WebSocket
function desconectar() {
    // Desativa o cliente Stomp
    clienteStomp.deactivate();
    // Configura o estado de conexão como falso
    setConectado(false);
    console.log("Desconectado");
}

// Função para enviar um nome ao servidor
function enviarNome() {
    clienteStomp.publish({
        destination: "/app/hello",
        body: JSON.stringify({ 'name': $("#nome").val() })
    });
}

// Função para mostrar uma saudação na interface
function mostrarSaudacao(mensagem) {
    $("#saudacoes").append("<tr><td>" + mensagem + "</td></tr>");
}

$(function () {
    // Impede o envio do formulário ao pressionar Enter
    $("form").on('submit', (e) => e.preventDefault());
    // Event listeners para os botões de conectar, desconectar e enviar
    $("#conectar").click(() => conectar());
    $("#desconectar").click(() => desconectar());
    $("#enviar").click(() => enviarNome());
});
