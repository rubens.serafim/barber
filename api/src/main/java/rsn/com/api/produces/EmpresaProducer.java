package rsn.com.api.produces;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import rsn.com.api.dtos.email.EmailDto;
import rsn.com.api.dtos.empresa.EmpresaDto;
import rsn.com.api.models.empresa.EmpresaModel;

@Component
public class EmpresaProducer {

    final RabbitTemplate rabbitTemplate;

    public EmpresaProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Value(value = "${broker.queue.email.name}")
    private String routingKey;

    public void publishMessageEmail(EmpresaModel empresaModel){
        
        var emailDto = new EmailDto();
        emailDto.setUuid(empresaModel.getUuid());
        emailDto.setNome(empresaModel.getNome());
        emailDto.setSubject("Cadastro realizado com sucesso");
        emailDto.setText(empresaModel.getNome() + "Seja bewm vindo! \n Agradecemos o cadastro" );

        rabbitTemplate.convertAndSend("", routingKey, emailDto);

    }

}



// package com.ms.user.produces;

// import org.springframework.amqp.rabbit.core.RabbitTemplate;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.stereotype.Component;

// import com.ms.user.dtos.EmailDto;
// import com.ms.user.model.empresaModel;

// @Component
// public class UserProducer {

//     final RabbitTemplate rabbitTemplate;

//     public UserProducer(RabbitTemplate rabbitTemplate) {
//         this.rabbitTemplate = rabbitTemplate;
//     }

//     @Value(value = "${broker.queue.email.name}")
//     private String routingKey;

//     public void publishMessageEmail(empresaModel empresaModel){
        
//         var emailDto = new EmailDto();
//         emailDto.setUserId(empresaModel.getUserId());
//         emailDto.setEmailTo(empresaModel.getEmail());
//         emailDto.setSubject("Cadastro realizado com sucesso");
//         emailDto.setText(empresaModel.getNome() + "Seja bewm vindo! \n Agradecemos o cadastro" );

//         rabbitTemplate.convertAndSend("", routingKey, emailDto);

//     }

// }

