package rsn.com.api.service.servico;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rsn.com.api.models.servico.ServicoModel;
import rsn.com.api.repository.ServicoRepository;

@Service
public class ServicoServiceImpl {

    private final ServicoRepository servicoRepository;

    @Autowired
    public ServicoServiceImpl(ServicoRepository servicoRepository) {
        this.servicoRepository = servicoRepository;
    }

    @Transactional
    public ServicoModel atualizarServico(ServicoModel servico) {
        return servicoRepository.save(servico);
    }
}
