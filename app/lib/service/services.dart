import 'package:app/models/Usuario.dart';
import 'package:app/models/atendimento.dart';
import 'package:app/models/dtos/QtdClientePorBarberDto.dart';
import 'package:app/service/ApiService.dart';
import 'package:flutter/material.dart';

class AuthService {
  // Método para cadastrar usuário
  static Future<Usuario?> cadastrarUsuario(Usuario usuario) async {
    try {
      // Chamada para a API para cadastrar o usuário
      final response = await ApiService.post('/usuarios/cadastrar', usuario.toJson());
      return Usuario.fromJson(response);
    } catch (e) {
      return null; // Retorna null se houver erro
    }
  }

  // Método para buscar usuários
  static Future<List<Usuario>> getUsuarios() async {
    final response = await ApiService.get('/usuarios');
    return (response as List).map((usuario) => Usuario.fromJson(usuario)).toList();
  }

  // Método para contar clientes por barbeiro
  static Future<List<QtdClientePorBarberDto>> contagemClientesPorBarbeiro() async {
    final response = await ApiService.get('/contagem-clientes');
    return (response as List).map((item) => QtdClientePorBarberDto.fromJson(item)).toList();
  }

  // Método para entrar na fila
  static Future<void> entrarNaFila(String barberUuid, String clienteUuid) async {
    await ApiService.post('/fila', {'barberUuid': barberUuid, 'clienteUuid': clienteUuid});
  }

  // Método para login
  static Future<String?> login(String email, String senha) async {
    final response = await ApiService.post('/login', {'email': email, 'senha': senha});
    return response['token']; // Supondo que o token vem nesse formato
  }


  static Future<Map<String, dynamic>> getUserDataFromToken() async {
    // Implementação para buscar dados do usuário
    final response = await ApiService.get('/user/data'); // Ajuste o endpoint
    return response;
  }

  // Método para buscar todos os atendimentos
  static Future<List<AtendimentoModel>> getAllAtendimentos() async {
    final response = await ApiService.get('/atendimentos'); // Ajuste o endpoint
    return (response as List).map((atendimento) => AtendimentoModel.fromJson(atendimento)).toList();
  }


}
