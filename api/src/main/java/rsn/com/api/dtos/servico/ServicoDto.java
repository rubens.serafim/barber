package rsn.com.api.dtos.servico;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class ServicoDto {
    
    private String id;
    private String servico;
    private String descricao;
    private BigDecimal valor;
}
