package rsn.com.api.service.usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rsn.com.api.exceptions.CPFAlreadyExistsException;
import rsn.com.api.exceptions.UsuarioNotFoundException;
import rsn.com.api.models.enums.UsuarioRole;
import rsn.com.api.models.usuario.UsuarioBarbeiroModel;
import rsn.com.api.produces.EmpresaProducer;
import rsn.com.api.repository.UsuarioBarbeiroRepository;

@Service
public class UsuarioBarbeiroServiceImpl {

    final UsuarioBarbeiroRepository usuarioBarbeiroRepository;

    final EmpresaProducer empresaProducer;

    public UsuarioBarbeiroServiceImpl(UsuarioBarbeiroRepository usuarioBarbeiroRepository, EmpresaProducer empresaProducer) {
        this.usuarioBarbeiroRepository = usuarioBarbeiroRepository;
        this.empresaProducer = empresaProducer;
    }

    @Transactional
    public UsuarioBarbeiroModel save(UsuarioBarbeiroModel usuarioBarbeiroModel) {
        // Verifica se já existe uma empresa com o CPF fornecido
        if (usuarioBarbeiroRepository.findByCpf(usuarioBarbeiroModel.getCpf()).isPresent()) {
            throw new CPFAlreadyExistsException(usuarioBarbeiroModel.getCpf());
        }

        usuarioBarbeiroModel = usuarioBarbeiroRepository.save(usuarioBarbeiroModel);
        // empresaProducer.publishMessageEmail(usuarioModel);
        return usuarioBarbeiroModel;
    }

    @Transactional(readOnly = true)
    public List<UsuarioBarbeiroModel> findAll() {
        return usuarioBarbeiroRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<UsuarioBarbeiroModel> findById(String id) {
        return usuarioBarbeiroRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public Optional<UsuarioBarbeiroModel> findByCpf(String cpf) {
        return usuarioBarbeiroRepository.findByCpf(cpf);
    }

    @Transactional
    public void atualizarTipoUsuario(UsuarioBarbeiroModel usuarioBarbeiroModel) {
        UsuarioBarbeiroModel usuario = usuarioBarbeiroRepository.findByCpf(usuarioBarbeiroModel.getCpf())
                .orElseThrow(() -> new UsuarioNotFoundException(
                        "Usuário não encontrado com o CPF: " + usuarioBarbeiroModel.getCpf()));
        usuario.setRole(usuarioBarbeiroModel.getRole());
        usuarioBarbeiroRepository.save(usuario);
    }

    public List<UsuarioBarbeiroModel> getUsuariosByTipo(UsuarioRole role) {
        return usuarioBarbeiroRepository.findByRoleOrderByNomeDesc(role);
    }

    @Transactional
    public UsuarioBarbeiroModel update(UsuarioBarbeiroModel usuarioBarbeiroModel) {
        /*
         * Verifica se a empresa existe antes de tentar atualizá-la
         */
        Optional<UsuarioBarbeiroModel> existingEmpresa = usuarioBarbeiroRepository.findById(usuarioBarbeiroModel.getUuid());
        if (existingEmpresa.isPresent()) {
            return usuarioBarbeiroRepository.save(usuarioBarbeiroModel);
        } else {
            /*
             * Lógica de tratamento para empresa não encontrada
             */
            throw new RuntimeException("Usuário " + usuarioBarbeiroModel.getNome() + "não encontrada com o ID fornecido: "
                    + usuarioBarbeiroModel.getUuid());
        }
    }

    @Transactional
    public void deleteById(String id) {
        // Verifica se a empresa existe antes de tentar excluí-la
        Optional<UsuarioBarbeiroModel> existingEmpresa = usuarioBarbeiroRepository.findById(id);
        if (existingEmpresa.isPresent()) {
            usuarioBarbeiroRepository.deleteById(id);
        } else {
            // Lógica de tratamento para empresa não encontrada
            throw new RuntimeException("Usuário não encontrada com o ID fornecido: " + id);
        }
    }
}
