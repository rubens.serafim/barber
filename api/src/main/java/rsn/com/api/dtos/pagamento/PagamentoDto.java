package rsn.com.api.dtos.pagamento;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class PagamentoDto {
    
    private String id;
    private String fkServico;
    private BigDecimal valor;
    // private String fkUsuarioCliente;
    // private String fkUsuarioBarber;
    private LocalDateTime dataHora;
    private String status;
}
