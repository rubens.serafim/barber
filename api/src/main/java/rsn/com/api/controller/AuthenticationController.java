// package rsn.com.api.controller;

// import jakarta.validation.Valid;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.http.ResponseEntity;
// import org.springframework.security.authentication.AuthenticationManager;
// import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RestController;

// import rsn.com.api.configs.infra.security.models.AuthenticationDTO;
// import rsn.com.api.configs.infra.security.models.LoginResponseDTO;
// import rsn.com.api.models.usuario.UsuarioModel;
// import rsn.com.api.repository.UsuarioRepository;
// import rsn.com.api.service.authorization.TokenService;

// @RestController
// @RequestMapping("auth")
// public class AuthenticationController {
//     @Autowired
//     private AuthenticationManager authenticationManager;
//     @Autowired
//     private UsuarioRepository repository; 
//     @Autowired
//     private TokenService tokenService;

//     @PostMapping("/login")
//     public ResponseEntity login(@RequestBody @Valid AuthenticationDTO data) {
//         var usernamePassword = new UsernamePasswordAuthenticationToken(data.email(), data.senha());
//         var auth = this.authenticationManager.authenticate(usernamePassword);

//         var token = tokenService.generateToken((UsuarioModel) auth.getPrincipal());

//         return ResponseEntity.ok(new LoginResponseDTO(token));
//     }

//     @PostMapping("/register")
//     public ResponseEntity register(@RequestBody @Valid UsuarioModel data) {

//         if (this.repository.findByEmail(data.getEmail()) != null)
//             return ResponseEntity.badRequest().build();

//         String encryptedPassword = new BCryptPasswordEncoder().encode(data.getSenha());

//         data.setSenha(encryptedPassword);

//         this.repository.save(data);

//         return ResponseEntity.ok().build();
//     }
// }
