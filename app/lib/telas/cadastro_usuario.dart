import 'package:app/models/Usuario.dart';
import 'package:app/models/enums/UsuarioRole.dart';
import 'package:app/service/UsuarioService.dart';
import 'package:app/service/services.dart';
import 'package:app/telas/login.dart';
import 'package:app/util/serviceFormatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CadastroUsuario extends StatefulWidget {
  const CadastroUsuario({Key? key}) : super(key: key);

  @override
  _CadastroUsuarioState createState() => _CadastroUsuarioState();
}

class _CadastroUsuarioState extends State<CadastroUsuario> {
  final TextEditingController _nomeController = TextEditingController();
  final TextEditingController _cpfController = TextEditingController();
  final TextEditingController _celularController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _senhaController = TextEditingController();

  bool _isLoading = false;
  bool _obscureText = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            colors: [
              Colors.blue[900]!,
              Colors.blue[800]!,
              Colors.blue[400]!,
            ],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            const Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: Text(
                      "Sistema Barbearia",
                      style: TextStyle(color: Colors.white, fontSize: 40),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  ),
                ),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        TextField(
                          controller: _nomeController,
                          decoration: const InputDecoration(
                            labelText: 'Nome',
                          ),
                          maxLength: 45,
                        ),
                        TextField(
                          controller: _cpfController,
                          decoration: const InputDecoration(labelText: 'CPF'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          onChanged: (value) {
                            setState(() {
                              _cpfController.text =
                                  ServiceFormatter.cpfInputFormatter(value);
                              _cpfController.selection =
                                  TextSelection.fromPosition(
                                TextPosition(
                                    offset: _cpfController.text.length),
                              );
                            });
                          },
                          maxLength: 11,
                        ),
                        TextField(
                          controller: _celularController,
                          decoration:
                              const InputDecoration(labelText: 'Celular'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          onChanged: (value) {
                            setState(() {
                              _celularController.text =
                                  ServiceFormatter.celularInputFormatter(value);
                              _celularController.selection =
                                  TextSelection.fromPosition(
                                TextPosition(
                                    offset: _celularController.text.length),
                              );
                            });
                          },
                          maxLength: 11,
                        ),
                        TextField(
                          controller: _emailController,
                          decoration: InputDecoration(
                            labelText: 'E-mail',
                            errorText: _emailController.text.isNotEmpty &&
                                    !ServiceFormatter.validateEmail(
                                        _emailController.text)
                                ? 'E-mail inválido'
                                : null,
                          ),
                        ),
                        TextField(
                          controller: _senhaController,
                          decoration: InputDecoration(
                            labelText: 'Senha',
                            suffixIcon: IconButton(
                              icon: Icon(
                                _obscureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Colors.grey,
                              ),
                              onPressed: () {
                                setState(() {
                                  _obscureText = !_obscureText;
                                });
                              },
                            ),
                          ),
                          obscureText: _obscureText,
                          maxLength: 15,
                        ),
                        const SizedBox(height: 80),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.blue,
                              foregroundColor: Colors.white,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 80.0),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                            ),
                            icon: const Icon(Icons.add_reaction),
                            label: Text(
                              "Cadastrar".toUpperCase(),
                              style: const TextStyle(color: Colors.white),
                            ),
                            onPressed: _isLoading
                                ? null
                                : () async {
                                    if (!camposVazios()) {
                                      setState(() {
                                        _isLoading = true;
                                      });
                                      _cadastrarUsuario();
                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                          content: Text(
                                            "Preencha todos os campos",
                                            textAlign: TextAlign.center,
                                          ),
                                          backgroundColor: Colors.blue,
                                        ),
                                      );
                                    }
                                  },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  bool camposVazios() {
    bool vazio = _nomeController.text.isEmpty ||
        _cpfController.text.isEmpty ||
        _senhaController.text.isEmpty ||
        _celularController.text.isEmpty ||
        _emailController.text.isEmpty;
    return vazio;
  }

  void _cadastrarUsuario() async {
    Usuario usuario = Usuario(
        uuid: '',
        nome: _nomeController.text,
        cpf: _cpfController.text,
        celular: _celularController.text,
        email: _emailController.text,
        senha: _senhaController.text,
        foto: '',
        status: '',
        role: UsuarioRole.USER,
        dataCriacao: DateTime.now(),
        quantidadeClientes: '');

    await UsuarioService.cadastrarUsuario(usuario).then((value) => {
          if (value != null)
            {
              setState(() {
                _isLoading = false;
              }),
              // Mostrar o diálogo de sucesso
              dialogoConfirmacao(),
            }
          else
            {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text(
                    "Usuário já cadastrado!",
                    textAlign: TextAlign.center,
                  ),
                  backgroundColor: Colors.blue,
                ),
              )
            }
        });
  }

  Future<dynamic> dialogoConfirmacao() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Usuário cadastrado com sucesso'),
          content: const Text('Verifique seu e-mail para confirmar cadastro!'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.push<void>(
                  context,
                  MaterialPageRoute<void>(
                    builder: (BuildContext context) => const Login(),
                  ),
                );
              },
              child: const Text('OK'),
            ),
          ],
        );
      },
    );
  }
}
