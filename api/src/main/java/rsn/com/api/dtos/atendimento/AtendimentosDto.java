package rsn.com.api.dtos.atendimento;

import lombok.Data;
import lombok.NoArgsConstructor;
import rsn.com.api.models.enums.StatusAtendimento;
import rsn.com.api.models.pagamento.PagamentoModel;
import rsn.com.api.models.usuario.UsuarioModel;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
public class AtendimentosDto {

     private String uuid;

    private PagamentoModel pagamento;

    private UsuarioModel usuarioBarber;

    private UsuarioModel usuarioCliente;

    private LocalDateTime dataHoraInicio;

    private LocalDateTime dataHoraInicioAtendimento;

    private LocalDateTime dataHoraFimAtendimento;

    private LocalDateTime dataHoraFimPagamento;

    private StatusAtendimento status;
}
