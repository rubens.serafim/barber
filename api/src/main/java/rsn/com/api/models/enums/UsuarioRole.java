package rsn.com.api.models.enums;

public enum UsuarioRole {

    ADMIN("ADMIN"),
    BARBEIRO("BARBEIRO"),
    CLIENTE("CLIENTE"),
    CAIXA("CAIXA"),
    USER("USER");

    private String role;

    UsuarioRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}