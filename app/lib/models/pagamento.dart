import 'package:app/models/ServicoModel.dart';
import 'package:app/models/enums/status_pagamento.dart';

class PagamentoModel {
  String uuid;
  ServicoModel servico;
  DateTime dataHora;
  int valor;
  StatusPagamento status;

  PagamentoModel({
    required this.uuid,
    required this.servico,
    required this.dataHora,
    required this.valor,
    required this.status,
  });

  PagamentoModel.fromJson(Map<String, dynamic> json)
      : uuid = json['uuid'],
        servico = ServicoModel.fromJson(json['servico']),
        dataHora = DateTime.parse(json['dataHora']),
        valor = int.parse(json['valor']),
        status = json['status'];

  Map<String, dynamic> toJson() => {
        'uuid': uuid,
        'servico': servico.toJson(),
        'dataHora': dataHora.toIso8601String(),
        'valor': valor.toString(),
        'status': status,
      };
}
