import 'package:app/models/Usuario.dart';
import 'package:app/models/enums/status_atendimento.dart';
import 'package:app/models/pagamento.dart';

class AtendimentoModel {
  String uuid;
  // PagamentoModel? pagamento;
  Usuario usuarioBarber;
  Usuario usuarioCliente;
  DateTime? dataHoraInicio;
  DateTime? dataHoraInicioAtendimento;
  DateTime? dataHoraFimAtendimento;
  DateTime? dataHoraFimPagamento;
  StatusAtendimento? status;

  AtendimentoModel({
    required this.uuid,
    // this.pagamento,
    required this.usuarioBarber,
    required this.usuarioCliente,
    this.dataHoraInicio,
    this.dataHoraInicioAtendimento,
    this.dataHoraFimAtendimento,
    this.dataHoraFimPagamento,
    this.status,
  });

  factory AtendimentoModel.fromJson(Map<String, dynamic> json) {
    return AtendimentoModel(
      uuid: json['uuid'] ?? '',
      // pagamento: json['pagamento'] != null ? PagamentoModel.fromJson(json['pagamento']) : null,
      usuarioBarber: Usuario.fromJson(json['usuarioBarber'] ?? {}),
      usuarioCliente: Usuario.fromJson(json['usuarioCliente'] ?? {}),
      dataHoraInicio: json['dataHoraInicio'] != null
          ? DateTime.parse(json['dataHoraInicio'])
          : null,
      dataHoraInicioAtendimento: json['dataHoraInicioAtendimento'] != null
          ? DateTime.parse(json['dataHoraInicioAtendimento'])
          : null,
      dataHoraFimAtendimento: json['dataHoraFimAtendimento'] != null
          ? DateTime.parse(json['dataHoraFimAtendimento'])
          : null,
      dataHoraFimPagamento: json['dataHoraFimPagamento'] != null
          ? DateTime.parse(json['dataHoraFimPagamento'])
          : null,
      status: json['status'] != null
          ? StatusAtendimento.values.firstWhere(
              (e) => e.toString() == 'StatusAtendimento.${json['status']}')
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'uuid': uuid,
      // 'pagamento': pagamento?.toJson(),
      'usuarioBarber': usuarioBarber.toJson(),
      'usuarioCliente': usuarioCliente.toJson(),
      'dataHoraInicio': dataHoraInicio?.toIso8601String(),
      'dataHoraInicioAtendimento': dataHoraInicioAtendimento?.toIso8601String(),
      'dataHoraFimAtendimento': dataHoraFimAtendimento?.toIso8601String(),
      'dataHoraFimPagamento': dataHoraFimPagamento?.toIso8601String(),
      'status': status?.toString().split('.').last,
    };
  }

  @override
  String toString() {
    return 'AtendimentoModel(uuid: $uuid, '
        'usuarioBarber: ${usuarioBarber.nome}, '
        'usuarioCliente: ${usuarioCliente.nome}, '
        'dataHoraInicio: $dataHoraInicio, '
        'dataHoraInicioAtendimento: $dataHoraInicioAtendimento, '
        'dataHoraFimAtendimento: $dataHoraFimAtendimento, '
        'dataHoraFimPagamento: $dataHoraFimPagamento, '
        'status: $status)';
  }
}
