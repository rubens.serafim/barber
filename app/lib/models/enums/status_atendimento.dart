enum StatusAtendimento {
  AGUARDANDO,
  ATENDIMENTO,
  ATENDIDO,
  DESISTIU,
}

extension StatusAtendimentoExtension on StatusAtendimento {
  String get descricao {
    switch (this) {
      case StatusAtendimento.AGUARDANDO:
        return 'AGUARDANDO';
      case StatusAtendimento.ATENDIMENTO:
        return 'ATENDIMENTO';
      case StatusAtendimento.ATENDIDO:
        return 'ATENDIDO';
      case StatusAtendimento.DESISTIU:
        return 'DESISTIU';
      default:
        throw Exception('Descrição não encontrada para o status');
    }
  }
}
