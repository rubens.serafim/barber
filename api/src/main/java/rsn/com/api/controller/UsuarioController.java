
package rsn.com.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rsn.com.api.configs.infra.security.models.AuthenticationDTO;
import rsn.com.api.configs.infra.security.models.LoginResponseDTO;
import rsn.com.api.exceptions.CPFAlreadyExistsException;
import rsn.com.api.models.enums.UsuarioRole;
import rsn.com.api.models.usuario.UsuarioModel;
import rsn.com.api.repository.UsuarioRepository;
import rsn.com.api.service.authorization.TokenService;
import rsn.com.api.service.usuario.UsuarioServiceImpl;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    final UsuarioServiceImpl usuarioService;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UsuarioRepository repository;
    @Autowired
    private TokenService tokenService;

    public UsuarioController(UsuarioServiceImpl usuarioService) {
        this.usuarioService = usuarioService;
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody @Valid AuthenticationDTO data) {
        var usernamePassword = new UsernamePasswordAuthenticationToken(data.email(), data.senha());
        var auth = this.authenticationManager.authenticate(usernamePassword);

        var token = tokenService.generateToken((UsuarioModel) auth.getPrincipal());

        return ResponseEntity.ok(new LoginResponseDTO(token));
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody @Valid UsuarioModel data) {

        if (this.repository.findByEmail(data.getEmail()) != null)
            return ResponseEntity.badRequest().build();

        String encryptedPassword = new BCryptPasswordEncoder().encode(data.getSenha());

        data.setSenha(encryptedPassword);

        this.repository.save(data);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<?> cadastrarUsuario(@RequestBody UsuarioModel usuarioModel) {

        try {
            if (this.repository.findByEmail(usuarioModel.getEmail()) != null)
                return ResponseEntity.badRequest().build();

            String encryptedPassword = new BCryptPasswordEncoder().encode(usuarioModel.getSenha());

            usuarioModel.setSenha(encryptedPassword);

            UsuarioModel usuarioModelSalva = usuarioService.save(usuarioModel);
            return ResponseEntity.status(HttpStatus.CREATED).body(usuarioModelSalva);
        } catch (CPFAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Erro ao cadastrar usuario: " + e.getMessage());
        }
    }

    @GetMapping("usuarioId/{id}")
    public ResponseEntity<UsuarioModel> getUsuarioById(@PathVariable String id) {
        Optional<UsuarioModel> usuarioModel = usuarioService.findById(id);
        return usuarioModel.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("usuarioCpf/{cpf}")
    public ResponseEntity<UsuarioModel> getUsuarioByCpf(@PathVariable String cpf) {
        Optional<UsuarioModel> usuarioModel = usuarioService.findByCpf(cpf);
        return usuarioModel.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public List<UsuarioModel> getAllUsuarios() {
        return usuarioService.findAll();
    }

    @PutMapping("/{id}")
    public ResponseEntity<UsuarioModel> updateUsuario(@PathVariable String id, @RequestBody UsuarioModel usuarioModel) {
        usuarioModel.setUuid(id);
        try {
            UsuarioModel updatedUsuario = usuarioService.update(usuarioModel);
            return ResponseEntity.ok(updatedUsuario);
        } catch (RuntimeException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/usuario/atualizar")
    public void atualizarTipoUsuario(@RequestBody UsuarioModel usuarioModel) {
        usuarioService.atualizarTipoUsuario(usuarioModel);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteUsuario(@PathVariable String id) {
        try {
            usuarioService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/barbeiros")
    public List<UsuarioModel> getBarbeiros() {
        return usuarioService.getUsuariosByTipo(UsuarioRole.BARBEIRO);
    }

    @GetMapping("/clientes")
    public List<UsuarioModel> getClientes() {
        return usuarioService.getUsuariosByTipo(UsuarioRole.CLIENTE);
    }

    @GetMapping("/administradores")
    public List<UsuarioModel> getAdministradores() {
        return usuarioService.findAll();
    }

    @GetMapping("/caixas")
    public List<UsuarioModel> getCaixas() {
        return usuarioService.getUsuariosByTipo(UsuarioRole.CLIENTE);
    }

}
