package rsn.com.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rsn.com.api.exceptions.CNPJAlreadyExistsException;
import rsn.com.api.models.empresa.EmpresaModel;
import rsn.com.api.service.empresa.EmpresaServiceImpl;

/**
 * Controlador para manipular requisições relacionadas a empresas.
 */
@RestController
@RequestMapping("/empresas")
public class EmpresaController {

    private final EmpresaServiceImpl empresaService;

    public EmpresaController(EmpresaServiceImpl empresaService) {
        this.empresaService = empresaService;
    }

    /**
     * Endpoint para cadastrar uma nova empresa.
     *
     * @param empresaRecordDto O objeto EmpresaModel contendo os dados da empresa a
     *                         ser cadastrada.
     * @return Resposta HTTP com a empresa cadastrada e status 201 (Created).
     */
    @PostMapping("/cadastrar")
    public ResponseEntity<?> cadastrarEmpresa(@RequestBody EmpresaModel empresaRecordDto) {

        try {
            EmpresaModel empresaModel = empresaService.save(empresaRecordDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(empresaModel);
        } catch (CNPJAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Erro ao cadastrar empresa: " + e.getMessage());
        }
    }

    /**
     * Endpoint para obter uma empresa pelo seu ID.
     *
     * @param id O ID da empresa.
     * @return Resposta HTTP com a empresa encontrada e status 200 (OK), ou status
     *         404 (Not Found) se não encontrada.
     */
    @GetMapping("/{id}")
    public ResponseEntity<EmpresaModel> getEmpresaById(@PathVariable String id) {
        Optional<EmpresaModel> empresaModel = empresaService.findById(id);
        return empresaModel.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Endpoint para obter todas as empresas.
     *
     * @return Lista de empresas encontradas.
     */
    @GetMapping
    public List<EmpresaModel> getAllEmpresas() {
        return empresaService.findAll();
    }

    /**
     * Endpoint para atualizar uma empresa.
     *
     * @param id           O ID da empresa a ser atualizada.
     * @param empresaModel O objeto EmpresaModel contendo os novos dados da empresa.
     * @return Resposta HTTP com a empresa atualizada e status 200 (OK), ou status
     *         404 (Not Found) se não encontrada.
     */
    @PutMapping("/{id}")
    public ResponseEntity<EmpresaModel> updateEmpresa(@PathVariable String id, @RequestBody EmpresaModel empresaModel) {
        empresaModel.setUuid(id);
        try {
            EmpresaModel updatedEmpresa = empresaService.update(empresaModel);
            return ResponseEntity.ok(updatedEmpresa);
        } catch (RuntimeException e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Endpoint para excluir uma empresa.
     *
     * @param id O ID da empresa a ser excluída.
     * @return Resposta HTTP com status 204 (No Content) se a exclusão for
     *         bem-sucedida,
     *         ou status 400 (Bad Request) se ocorrer um erro durante a exclusão.
     */
    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteEmpresa(@PathVariable String id) {
        try {
            empresaService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
