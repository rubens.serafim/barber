package rsn.com.api.models.usuario;

import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import rsn.com.api.models.enums.UsuarioRole;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "tb_usuarios")
public class UsuarioModel implements UserDetails {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "pk_usuario", updatable = false, nullable = false)
    private String uuid;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "celular")
    private String celular;

    @Column(name = "email")
    private String email;

    @Column(name = "senha")
    private String senha;

    @Column(name = "foto")
    private String foto;

    @Column(name = "status")
    private String status;

    private UsuarioRole role;

    @Column(name = "data_criacao", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime dataCriacao;

    @Column(name = "data_exclusao")
    private LocalDateTime dataExclusao;

    public UsuarioModel(String email, String senha, UsuarioRole role) {
        this.email = email;
        this.senha = senha;
        this.role = role;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (this.role == UsuarioRole.ADMIN)
            return List.of(
                new SimpleGrantedAuthority("ROLE_ADMIN"),
                new SimpleGrantedAuthority("ROLE_USER")
                );
        else
            return List.of(new SimpleGrantedAuthority("ROLE_USER"));
    }

    // @Override
    // public Collection<? extends GrantedAuthority> getAuthorities() {
    //     List<SimpleGrantedAuthority> authorities = new ArrayList<>();

    //     // Adicionar a autoridade ROLE_USER independentemente do papel
    //     authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

    //     // Adicionar outras autoridades com base nos papéis do usuário
    //     if (this.role != null) {
    //         // Converte a enumeração para uma lista
    //         List<UsuarioRole> rolesList = Arrays.asList(this.role);

    //         // Itera sobre a lista de papéis e adiciona as autoridades correspondentes
    //         for (UsuarioRole role : rolesList) {
    //             authorities.add(new SimpleGrantedAuthority("ROLE_" + role.name()));
    //         }
    //     }

    //     return authorities;
    // }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return email;

    }

}
