
package rsn.com.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rsn.com.api.exceptions.CPFAlreadyExistsException;
import rsn.com.api.models.usuario.UsuarioBarbeiroModel;
import rsn.com.api.repository.UsuarioBarbeiroRepository;
import rsn.com.api.repository.UsuarioRepository;
import rsn.com.api.service.usuario.UsuarioBarbeiroServiceImpl;

@RestController
@RequestMapping("/barbeiro")
public class UsuarioBarbeiroController {

    final UsuarioBarbeiroServiceImpl usuarioBarbeiroService;

    @Autowired
    private UsuarioBarbeiroRepository repository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public UsuarioBarbeiroController(UsuarioBarbeiroServiceImpl usuarioBarbeiroService) {
        this.usuarioBarbeiroService = usuarioBarbeiroService;
    }

   
    @PostMapping("/cadastrar")
    public ResponseEntity<?> cadastrarUsuario(@RequestBody UsuarioBarbeiroModel usuarioBarbeiroModel) {

        try {
            if (this.usuarioRepository.findByEmail(usuarioBarbeiroModel.getEmail()) == null)
                return ResponseEntity.badRequest().build();

            String encryptedPassword = new BCryptPasswordEncoder().encode(usuarioBarbeiroModel.getSenha());

            usuarioBarbeiroModel.setSenha(encryptedPassword);

            UsuarioBarbeiroModel usuarioBarbeiroModelSalva = usuarioBarbeiroService.save(usuarioBarbeiroModel);
            return ResponseEntity.status(HttpStatus.CREATED).body(usuarioBarbeiroModelSalva);
        } catch (CPFAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Erro ao cadastrar barbeiro: " + e.getMessage());
        }
    }

    @GetMapping("usuarioId/{id}")
    public ResponseEntity<UsuarioBarbeiroModel> getUsuarioById(@PathVariable String id) {
        Optional<UsuarioBarbeiroModel> usuarioBarbeiroModel = usuarioBarbeiroService.findById(id);
        return usuarioBarbeiroModel.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("usuarioCpf/{cpf}")
    public ResponseEntity<UsuarioBarbeiroModel> getUsuarioByCpf(@PathVariable String cpf) {
        Optional<UsuarioBarbeiroModel> usuarioBarbeiroModel = usuarioBarbeiroService.findByCpf(cpf);
        return usuarioBarbeiroModel.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public List<UsuarioBarbeiroModel> getAllUsuarios() {
        return usuarioBarbeiroService.findAll();
    }

    @PutMapping("/{id}")
    public ResponseEntity<UsuarioBarbeiroModel> updateUsuario(@PathVariable String id, @RequestBody UsuarioBarbeiroModel usuarioBarbeiroModel) {
        usuarioBarbeiroModel.setUuid(id);
        try {
            UsuarioBarbeiroModel updatedUsuarioBarbeiro = usuarioBarbeiroService.update(usuarioBarbeiroModel);
            return ResponseEntity.ok(updatedUsuarioBarbeiro);
        } catch (RuntimeException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/usuario/atualizar")
    public void atualizarTipoUsuario(@RequestBody UsuarioBarbeiroModel usuarioBarbeiroModel) {
        usuarioBarbeiroService.atualizarTipoUsuario(usuarioBarbeiroModel);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteUsuario(@PathVariable String id) {
        try {
            usuarioBarbeiroService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }


}
