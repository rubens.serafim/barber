// Definição do enum UsuarioRole
enum UsuarioRole { ADMIN, BARBEIRO, CLIENTE, CAIXA, USER }

// Função para converter um índice em um UsuarioRole
// UsuarioRole usuarioRoleFromIndex(int index) {
//   print(index);

//   return UsuarioRole.values[index];
// }

// Função para converter um UsuarioRole em um índice
int usuarioRoleToIndex(UsuarioRole role) {
  return role.index;
}

// Função para converter um UsuarioRole para uma string
String usuarioRoleToString(UsuarioRole role) {
  switch (role) {
    case UsuarioRole.ADMIN:
      return 'ADMIN';
    case UsuarioRole.BARBEIRO:
      return 'BARBEIRO';
    case UsuarioRole.CLIENTE:
      return 'CLIENTE';
    case UsuarioRole.CAIXA:
      return 'CAIXA';
    case UsuarioRole.USER:
      return 'USER';
  }
}

// Função para converter uma string em um UsuarioRole
UsuarioRole usuarioRoleFromString(String str) {
  switch (str) {
    case 'ADMIN':
      return UsuarioRole.ADMIN;
    case 'BARBEIRO':
      return UsuarioRole.BARBEIRO;
    case 'CLIENTE':
      return UsuarioRole.CLIENTE;
    case 'CAIXA':
      return UsuarioRole.CAIXA;
    case 'USER':
      return UsuarioRole.USER;
    default:
      throw ArgumentError('Invalid UsuarioRole string: $str');
  }
}

// Função para converter um UsuarioRole para JSON
Map<String, dynamic> usuarioRoleToJson(UsuarioRole role) {
  return {
    'index': usuarioRoleToIndex(role),
  };
}

// Função para desserializar um UsuarioRole de JSON
// UsuarioRole usuarioRoleFromJson(Map<String, dynamic> json) {
//   return usuarioRoleFromIndex(json['index']);
// }
