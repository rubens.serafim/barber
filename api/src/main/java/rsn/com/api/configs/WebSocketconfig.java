package rsn.com.api.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * Configuração do WebSocket para a aplicação.
 */
@Configuration
@EnableWebSocket
public class WebSocketconfig implements WebSocketConfigurer {

	/**
	 * Registra manipuladores de WebSocket.
	 *
	 * @param registry O registro de manipuladores de WebSocket.
	 */
	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(manipuladorMensagens(), "/ws")
				.setAllowedOrigins("*");
	}

	/**
	 * Cria um manipulador de mensagens WebSocket.
	 *
	 * @return O manipulador de mensagens WebSocket criado.
	 */
	@Bean
	WebSocketHandler manipuladorMensagens() {
		return new ManipuladorMensagens();
	}
}
