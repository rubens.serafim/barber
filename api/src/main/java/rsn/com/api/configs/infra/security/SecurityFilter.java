package rsn.com.api.configs.infra.security;

import rsn.com.api.repository.UsuarioRepository;
import rsn.com.api.service.authorization.TokenService;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * Filtro de segurança para validar tokens de autenticação.
 */
@Component
public class SecurityFilter extends OncePerRequestFilter {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UsuarioRepository userRepository;

    /**
     * Método para realizar a filtragem das requisições HTTP.
     *
     * @param request     O objeto HttpServletRequest.
     * @param response    O objeto HttpServletResponse.
     * @param filterChain O filtro de cadeia de requisições.
     * @throws ServletException Se ocorrer um erro durante o processo de filtragem.
     * @throws IOException      Se ocorrer um erro de entrada/saída durante o
     *                          processo de filtragem.
     */
    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException {

        var token = this.recoverToken(request);

        if (token != null) {
            var email = tokenService.validateToken(token);
            UserDetails user = userRepository.findByEmail(email);

            var authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Método para recuperar o token de autenticação do cabeçalho da requisição.
     *
     * @param request O objeto HttpServletRequest.
     * @return O token de autenticação, ou null se não estiver presente.
     */
    private String recoverToken(HttpServletRequest request) {
        var authHeader = request.getHeader("Authorization");
        if (authHeader == null)
            return null;
        return authHeader.replace("Bearer ", "");
    }
}
