package rsn.com.api.dtos.atendimento;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class AtendimentoDto {

    private String id;
    private String fkPagamento;
    private String fkUsuarioBarber;
    private String fkUsuarioCliente;
    private LocalDateTime dataHoraInicio;
    private LocalDateTime dataHoraInicioAtendimento;
    private LocalDateTime dataHoraFimAtendimento;
    private LocalDateTime dataHoraFimPagamento;
    private String status;
}
