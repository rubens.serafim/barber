class LoginResponseDTO {
  final String token;

  LoginResponseDTO({required this.token});

  factory LoginResponseDTO.fromJson(Map<String, dynamic> json) {
    return LoginResponseDTO(
      token: json['token'],
    );
  }
}
