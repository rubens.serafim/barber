package rsn.com.api.service.empresa;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rsn.com.api.exceptions.CNPJAlreadyExistsException;
import rsn.com.api.models.empresa.EmpresaModel;
import rsn.com.api.produces.EmpresaProducer;
import rsn.com.api.repository.EmpresaRepository;

@Service
public class EmpresaServiceImpl {

    final EmpresaRepository empresaRepository;

    final EmpresaProducer empresaProducer;

    public EmpresaServiceImpl(EmpresaRepository empresaRepository, EmpresaProducer empresaProducer) {
        this.empresaRepository = empresaRepository;
        this.empresaProducer = empresaProducer;
    }

    @Transactional
    public EmpresaModel save(EmpresaModel empresaModel) {
        // Verifica se já existe uma empresa com o CNPJ fornecido
        if (empresaRepository.findByCnpj(empresaModel.getCnpj()).isPresent()) {
            throw new CNPJAlreadyExistsException(empresaModel.getCnpj());
        }

        empresaModel = empresaRepository.save(empresaModel);
        // empresaProducer.publishMessageEmail(empresaModel);
        return empresaModel;
    }

    @Transactional(readOnly = true)
    public List<EmpresaModel> findAll() {
        return empresaRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<EmpresaModel> findById(String id) {
        return empresaRepository.findById(id);
    }

    @Transactional
    public EmpresaModel update(EmpresaModel empresaModel) {
        /*
         * Verifica se a empresa existe antes de tentar atualizá-la
         */
        Optional<EmpresaModel> existingEmpresa = empresaRepository.findById(empresaModel.getUuid());
        if (existingEmpresa.isPresent()) {
            return empresaRepository.save(empresaModel);
        } else {
            /*
             * Lógica de tratamento para empresa não encontrada
             */
            throw new RuntimeException("Empresa " + empresaModel.getNome() + "não encontrada com o ID fornecido: "
                    + empresaModel.getUuid());
        }
    }

    @Transactional
    public void deleteById(String id) {
        // Verifica se a empresa existe antes de tentar excluí-la
        Optional<EmpresaModel> existingEmpresa = empresaRepository.findById(id);
        if (existingEmpresa.isPresent()) {
            empresaRepository.deleteById(id);
        } else {
            // Lógica de tratamento para empresa não encontrada
            throw new RuntimeException("Empresa não encontrada com o ID fornecido: " + id);
        }
    }
}
