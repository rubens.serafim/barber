import 'package:flutter/material.dart';

import '../service/services.dart';

class HomePage extends StatelessWidget {
  final String ownerName;
  final String clienteUuid;
  final String barberUuid;
  final String barberQuantidadeClientes;
  final String barberNome;

  const HomePage({
    Key? key,
    required this.ownerName,
    required this.clienteUuid,
    required this.barberUuid,
    required this.barberQuantidadeClientes,
    required this.barberNome,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            colors: [
              Colors.blue[900]!,
              Colors.blue[800]!,
              Colors.blue[400]!,
            ],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            const Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: Text(
                      "Sistema Barbearia",
                      style: TextStyle(color: Colors.white, fontSize: 40),
                    ),
                  ),
                  SizedBox(height: 20),
                  Center(
                    child: Text(
                      "Escolha a fila",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 5),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  ),
                ),
                child: Center(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(height: 40.0),
                      Text(
                        barberNome,
                        style: const TextStyle(
                            color: Colors.blue,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      Card(
                        margin: const EdgeInsets.only(
                            top: 16, bottom: 16, left: 36, right: 36),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 36, bottom: 36, left: 16, right: 16),
                          child: Row(
                            children: [
                              const Icon(Icons.supervised_user_circle_rounded,
                                  size: 90, color: Colors.blue),
                              const SizedBox(width: 36.0),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    // Center(
                                    //   child: Column(
                                    //     children: [
                                    Text(
                                      barberQuantidadeClientes,
                                      // textAlign: TextAlign.start,
                                      style: const TextStyle(
                                          color: Colors.blue,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    const Text(
                                      "Pessoas na fila",
                                      textAlign: TextAlign.end,
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    //     ],
                                    //   ),
                                    // ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const Center(
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Fila convencional e preferencial',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 60),
                              Text(
                                'Não há possibilidade de agendar horário de atendimento',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.blue, fontSize: 16.0),
                              ),
                            ]),
                      ),
                      const SizedBox(height: 20),
                      Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ElevatedButton.icon(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.blue,
                                foregroundColor: Colors.white,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 70.0),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                              icon: const Icon(Icons.add_task),
                              label: Text(
                                'Entrar na fila'.toUpperCase(),
                                style: const TextStyle(color: Colors.white),
                              ),
                              onPressed: () async {
                                await AuthService.entrarNaFila(
                                    barberUuid, clienteUuid);
                                // Adicione a ação desejada ao botão "Voltar"
                                // tem que passar o id do barbeiro e do usuário
                                // Navigator.pop(context);
                              },
                            ),
                          ),
                          const SizedBox(height: 30),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ElevatedButton.icon(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.white,
                                foregroundColor: Colors.blue,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 100.0),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                              icon: const Icon(Icons.arrow_back_ios),
                              label: Text(
                                'Voltar'.toUpperCase(),
                                style: const TextStyle(color: Colors.blue),
                              ),
                              onPressed: () {
                                // Adicione a ação desejada ao botão "Voltar"
                                Navigator.pop(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
