package rsn.com.api.service.usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rsn.com.api.exceptions.CPFAlreadyExistsException;
import rsn.com.api.exceptions.UsuarioNotFoundException;
import rsn.com.api.models.enums.UsuarioRole;
import rsn.com.api.models.usuario.UsuarioModel;
import rsn.com.api.produces.EmpresaProducer;
import rsn.com.api.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl {

    final UsuarioRepository usuarioRepository;

    final EmpresaProducer empresaProducer;

    public UsuarioServiceImpl(UsuarioRepository usuarioRepository, EmpresaProducer empresaProducer) {
        this.usuarioRepository = usuarioRepository;
        this.empresaProducer = empresaProducer;
    }

    @Transactional
    public UsuarioModel save(UsuarioModel usuarioModel) {
        // Verifica se já existe uma empresa com o CPF fornecido
        if (usuarioRepository.findByCpf(usuarioModel.getCpf()).isPresent()) {
            throw new CPFAlreadyExistsException(usuarioModel.getCpf());
        }

        usuarioModel = usuarioRepository.save(usuarioModel);
        // empresaProducer.publishMessageEmail(usuarioModel);
        return usuarioModel;
    }

    @Transactional(readOnly = true)
    public List<UsuarioModel> findAll() {
        return usuarioRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<UsuarioModel> findById(String id) {
        return usuarioRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public Optional<UsuarioModel> findByCpf(String cpf) {
        return usuarioRepository.findByCpf(cpf);
    }

    @Transactional
    public void atualizarTipoUsuario(UsuarioModel usuarioModel) {
        UsuarioModel usuario = usuarioRepository.findByCpf(usuarioModel.getCpf())
                .orElseThrow(() -> new UsuarioNotFoundException(
                        "Usuário não encontrado com o CPF: " + usuarioModel.getCpf()));
        usuario.setRole(usuarioModel.getRole());
        usuarioRepository.save(usuario);
    }

    public List<UsuarioModel> getUsuariosByTipo(UsuarioRole role) {
        return usuarioRepository.findByRoleOrderByNomeDesc(role);
    }

    @Transactional
    public UsuarioModel update(UsuarioModel usuarioModel) {
        /*
         * Verifica se a empresa existe antes de tentar atualizá-la
         */
        Optional<UsuarioModel> existingEmpresa = usuarioRepository.findById(usuarioModel.getUuid());
        if (existingEmpresa.isPresent()) {
            return usuarioRepository.save(usuarioModel);
        } else {
            /*
             * Lógica de tratamento para empresa não encontrada
             */
            throw new RuntimeException("Usuário " + usuarioModel.getNome() + "não encontrada com o ID fornecido: "
                    + usuarioModel.getUuid());
        }
    }

    @Transactional
    public void deleteById(String id) {
        // Verifica se a empresa existe antes de tentar excluí-la
        Optional<UsuarioModel> existingEmpresa = usuarioRepository.findById(id);
        if (existingEmpresa.isPresent()) {
            usuarioRepository.deleteById(id);
        } else {
            // Lógica de tratamento para empresa não encontrada
            throw new RuntimeException("Usuário não encontrada com o ID fornecido: " + id);
        }
    }
}
