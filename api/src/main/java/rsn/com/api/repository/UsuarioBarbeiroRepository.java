package rsn.com.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.UserDetails;

import rsn.com.api.models.enums.UsuarioRole;
import rsn.com.api.models.usuario.UsuarioBarbeiroModel;
import rsn.com.api.models.usuario.UsuarioModel;

public interface UsuarioBarbeiroRepository extends JpaRepository<UsuarioBarbeiroModel, String> {

    /**
     * Consulta para buscar Usuario Barbeiro por role ordenados por nome em ordem
     * decrescente
     * 
     * @param role regra 
     * @return List <UsuarioModel>
     */
    @Query("SELECT u FROM UsuarioBarbeiroModel u " +
            "WHERE u.role = :role OR u.nome = 'Geral' " +
            "ORDER BY u.nome DESC")
    List<UsuarioBarbeiroModel> findByRoleOrderByNomeDesc(@Param("role") UsuarioRole role);

    Optional<UsuarioBarbeiroModel> findByCpf(String cpf);

    UserDetails findByEmail(String email);

    Optional<UsuarioBarbeiroModel> findById(String id);

    List<UsuarioBarbeiroModel> findAll();

    void deleteById(String id);

}
