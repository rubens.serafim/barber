package rsn.com.api.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rsn.com.api.models.servico.ServicoModel;


@Repository
public interface ServicoRepository extends JpaRepository<ServicoModel, String> {
    // Se necessário, adicione métodos personalizados aqui
}

