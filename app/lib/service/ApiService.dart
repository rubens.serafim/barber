import 'dart:convert';
import 'package:http/http.dart' as http;

class ApiService {
  static const String baseUrl = 'http://10.0.0.158:8081'; // Substitua pela URL da sua API

  // Método GET
  static Future<dynamic> get(String endpoint) async {
    final response = await http.get(Uri.parse('$baseUrl$endpoint'));

    _handleResponse(response);
    return json.decode(response.body);
  }

  // Método POST
  static Future<dynamic> post(String endpoint, Map<String, dynamic> body) async {
    final response = await http.post(
      Uri.parse('$baseUrl$endpoint'),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(body),
    );

    _handleResponse(response);
    return json.decode(response.body);
  }

  // Tratamento de resposta
  static void _handleResponse(http.Response response) {
    if (response.statusCode < 200 || response.statusCode >= 300) {
      throw Exception('Falha na requisição: ${response.statusCode}');
    }
  }
}
