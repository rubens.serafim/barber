package rsn.com.api.exceptions;

public class AtendimentoNotFoundException extends RuntimeException{
    public AtendimentoNotFoundException(String uuid) {
        super("Já existe uma atendimemto cadastrada com o uuid fornecido: " + uuid);
    }
}
