class ServicoModel {
  final String uuid;
  final String servico;
  final String descricao;
  final double valor;

  ServicoModel({
    required this.uuid,
    required this.servico,
    required this.descricao,
    required this.valor,
  });

  factory ServicoModel.fromJson(Map<String, dynamic> json) {
    return ServicoModel(
      uuid: json['uuid'],
      servico: json['servico'],
      descricao: json['descricao'],
      valor: json['valor'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'uuid': uuid,
      'servico': servico,
      'descricao': descricao,
      'valor': valor,
    };
  }
}
