package rsn.com.api.service.authorization;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import rsn.com.api.models.usuario.UsuarioModel;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Serviço para geração e validação de tokens de autenticação.
 */
@Service
public class TokenService {
    @Value("${api.security.token.secret}")
    private String secret;

    /**
     * Gera um token de autenticação para o usuário.
     *
     * @param user O usuário para o qual o token será gerado.
     * @return O token gerado.
     * @throws RuntimeException Se ocorrer um erro ao gerar o token.
     */
    public String generateToken(UsuarioModel user) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            return JWT.create()
                    .withIssuer("auth-api")
                    .withSubject(user.getEmail())
                    .withClaim("uuid",user.getUuid())
                    .withClaim("nome",user.getNome())
                    .withClaim("email",user.getEmail())
                    .withClaim("cpf",user.getCpf())
                    .withClaim("celular",user.getCelular())
                    .withClaim("status",user.getStatus())
                    .withClaim("authorities",user.getAuthorities().toString())
                    .withClaim("role",user.getRole().toString())
                    .withExpiresAt(genExpirationDate())
                    .sign(algorithm);

        } catch (JWTCreationException exception) {
            throw new RuntimeException("Erro ao gerar token", exception);
        }
    }

    /**
     * Valida um token de autenticação.
     *
     * @param token O token a ser validado.
     * @return O email do usuário associado ao token, ou uma string vazia se o token
     *         for inválido.
     */
    public String validateToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            return JWT.require(algorithm)
                    .withIssuer("auth-api")
                    .build()
                    .verify(token)
                    .getSubject();
        } catch (JWTVerificationException exception) {
            return "";
        }
    }

    /**
     * Gera a data de expiração para o token.
     *
     * @return A data de expiração do token.
     */
    private Instant genExpirationDate() {
        return LocalDateTime.now().plusHours(2).toInstant(ZoneOffset.of("-03:00"));
    }
}
