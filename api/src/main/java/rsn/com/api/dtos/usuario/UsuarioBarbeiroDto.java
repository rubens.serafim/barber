package rsn.com.api.dtos.usuario;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class UsuarioBarbeiroDto {

    private String uuid;
    private String nome;
    private String celular;
    private String email;
    private String senha;
    private String foto;
    private String tipo;
    private String status;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataExclusao;
    
}
