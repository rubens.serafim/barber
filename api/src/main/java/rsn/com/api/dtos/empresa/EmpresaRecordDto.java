package rsn.com.api.dtos.empresa;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record EmpresaRecordDto( @NotBlank String nome, 
                    @NotBlank @Email String email) {

                        
    
}
