package rsn.com.api.service.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import rsn.com.api.repository.UsuarioRepository;

/**
 * Serviço para autenticação e autorização de usuários.
 */
@Service
public class AuthorizationService implements UserDetailsService {

    @Autowired
    private UsuarioRepository repository;

    /**
     * Carrega os detalhes do usuário pelo nome de usuário (email).
     *
     * @param username O nome de usuário (email) do usuário.
     * @return Detalhes do usuário carregados.
     * @throws UsernameNotFoundException Se o usuário não for encontrado.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByEmail(username);
    }
}
