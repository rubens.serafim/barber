package rsn.com.api.dtos.atendimento;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QtdClientePorBarberDto {

    private String fkUsuarioBarbeiro; 
    private Long quantidadeClientes;

    // Construtor
    public QtdClientePorBarberDto(String fkUsuarioBarbeiro, Long quantidadeClientes) {
        this.fkUsuarioBarbeiro = fkUsuarioBarbeiro;
        this.quantidadeClientes = quantidadeClientes;
    }
}
