package rsn.com.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import rsn.com.api.dtos.atendimento.QtdClientePorBarberDto;
import rsn.com.api.models.atendimento.AtendimentoModel;
import rsn.com.api.models.usuario.UsuarioModel;

@Repository
public interface AtendimentoRepository extends JpaRepository<AtendimentoModel, String> {

    @Query("SELECT NEW rsn.com.api.dtos.atendimento.QtdClientePorBarberDto(a.usuarioBarber.uuid, " +
            "COUNT(DISTINCT a.usuarioCliente.uuid)) " +
            "FROM AtendimentoModel a " +
            "GROUP BY a.usuarioBarber.uuid")
    List<QtdClientePorBarberDto> countClientesPorBarbeiros();

    boolean existsByUsuarioCliente(UsuarioModel usuarioCliente);

    List<AtendimentoModel> findByUsuarioClienteUuid(String usuarioClienteId);

    @Query("SELECT DISTINCT a.usuarioBarber FROM AtendimentoModel a")
    List<UsuarioModel> findAllDistinctBarbers();

    @Query("SELECT a FROM AtendimentoModel a WHERE a.usuarioBarber.uuid = :barberId")
    List<AtendimentoModel> findAllByBarberId(String barberId);

}
