import 'package:app/telas/cadastro_usuario.dart';
import 'package:app/telas/filas.dart';
import 'package:app/telas/login.dart';
import 'package:flutter/material.dart';

class AppRoutes {
  static const String login = '/';
  static const String fila = '/fila';
  static const String cadastroUsuario = '/cadastro';
  // Adicione outras rotas conforme necessário

  static final Map<String, WidgetBuilder> routes = {
    login: (context) => const Login(),
    fila: (context) => const Filas(),
    cadastroUsuario: (context) => CadastroUsuario(),
    // Adicione outras rotas conforme necessário
  };
}
