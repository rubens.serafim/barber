import 'package:app/models/Usuario.dart';
import 'package:app/models/atendimento.dart';
import 'package:app/models/dtos/LoginResponseDTO.dart';
import 'package:app/models/dtos/QtdClientePorBarberDto.dart';
import 'package:app/service/ApiService.dart';
import 'package:flutter/material.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

class UsuarioService {
  static const String baseUrl = 'http://10.0.0.158:8081';

  static Future<Usuario?> cadastrarUsuario(Usuario usuario) async {
    final response = await http.post(
      Uri.parse('$baseUrl/usuarios/cadastrar'),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(usuario.toJson()),
    );

    if (response.statusCode == 201) {
      // Se a requisição for bem-sucedida, retorna o usuário salvo
      return Usuario.fromJson(json.decode(response.body));
    } else {
      // Lidar com erros aqui
      print('Erro: ${response.statusCode} ${response.body}');
      return null; // Ou lançar uma exceção
    }
  }

  // Método para buscar usuários
  static Future<List<Usuario>> buscarUsuarioBarbeiro() async {
    final response = await ApiService.get('/usuarios');
    return (response as List)
        .map((usuario) => Usuario.fromJson(usuario))
        .toList();
  }

  // Método para login
  
 static Future<LoginResponseDTO?> login(String email, String senha) async {
    final response = await http.post(
      Uri.parse('$baseUrl/usuarios/login'),
      headers: {'Content-Type': 'application/json'},
      body: json.encode({'email': email, 'senha': senha}),
    );

    if (response.statusCode == 200) {
      return LoginResponseDTO.fromJson(json.decode(response.body));
    } else {
      // Trate o erro conforme necessário
      return null;
    }
  }

  static Future<Map<String, dynamic>> getUserDataFromToken() async {
    // Implementação para buscar dados do usuário
    final response = await ApiService.get('/user/data'); // Ajuste o endpoint
    return response;
  }

    // Método para contar clientes por barbeiro
  static Future<List<QtdClientePorBarberDto>> contagemClientesPorBarbeiro() async {
    final response = await ApiService.get('/contagem-clientes');
    return (response as List).map((item) => QtdClientePorBarberDto.fromJson(item)).toList();
  }

}
