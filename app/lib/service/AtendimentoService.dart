import 'package:app/models/Usuario.dart';
import 'package:app/models/atendimento.dart';
import 'package:app/models/dtos/LoginResponseDTO.dart';
import 'package:app/models/dtos/QtdClientePorBarberDto.dart';
import 'package:app/service/ApiService.dart';
import 'package:app/service/authStorage.dart';
import 'package:flutter/material.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

class AtendimentoService {
  static const String baseUrl = 'http://10.0.0.158:8081';

  static Future<Map<String, dynamic>> getUserDataFromToken(String token) async {
    final response = await http.get(
      Uri.parse('http://localhost:8080/api/usuario/login'),
      headers: {
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Falha ao buscar dados do usuário');
    }
  }

  static Future<List<AtendimentoModel>> getAllAtendimentos() async {
    final token = AuthStorage().token;
    final response = await http.get(
      Uri.parse('$baseUrl/atendimentos'),
      headers: {
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      // Convertemos a resposta para uma lista de objetos
      List<dynamic> jsonList = jsonDecode(response.body);

      // Convertemos cada item da lista para um `AtendimentoModel`
      List<AtendimentoModel> atendimentos = jsonList.map((json) {
        return AtendimentoModel.fromJson(json);
      }).toList();

      return atendimentos;
    } else {
      throw Exception('Falha ao buscar atendimentos');
    }
  }
}
